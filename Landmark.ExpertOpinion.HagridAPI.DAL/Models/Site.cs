﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HagridAPI.DAL.Models
{
    public class Site
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string LatLong { get; set; }
    }
}
