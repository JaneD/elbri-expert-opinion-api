﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HagridAPI.DAL.Models;

namespace HagridAPI.DAL.Repositories
{
    public class SiteRepository : ISiteRepository
    {
        private IQueryable<Site> fakeSites; 
        public SiteRepository()
        {
            fakeSites = new List<Site> {
                new Site()
                {
                    Id = 1,
                    Name = "Andys House",
                    LatLong = "BN11QD"
                }
            }.AsQueryable();
        }

        public Site GetSiteById(int id)
        {
            //TODO: Postgresql connection here
            return new Site();
        }

        public Site[] GetAll()
        {
            return fakeSites.ToArray();
        }
    }
}
