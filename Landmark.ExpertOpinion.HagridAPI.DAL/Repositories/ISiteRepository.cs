﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HagridAPI.DAL.Models;

namespace HagridAPI.DAL.Repositories
{
    public interface ISiteRepository
    {
        Site GetSiteById(int id);
        Site[] GetAll();
    }
}
