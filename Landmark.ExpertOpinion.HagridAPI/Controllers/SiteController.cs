﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using Autofac.Integration.WebApi;
using HagridAPI.DAL.Models;
using HagridAPI.DAL.Repositories;
using Newtonsoft.Json;

namespace HagridAPI.Controllers
{
    [AutofacControllerConfiguration]
    public class SiteController : ApiController
    {
        private ISiteRepository _repository;

        public SiteController(ISiteRepository siteRepository)
        {
            _repository = siteRepository;

            Console.Write("we made it here");
        }

        [Route("api/sites")]
        public IHttpActionResult GetAll()
        {
            return Ok(_repository.GetAll());
        }

        public IHttpActionResult Get(int id)
        {
            var site = _repository.GetSiteById(id);

            if (site == null)
            {
                return NotFound();
            }

            return Ok(site);
        }
    }
}
