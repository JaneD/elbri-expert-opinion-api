﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using HagridAPI.Controllers;
using HagridAPI.DAL.Models;
using HagridAPI.DAL.Repositories;
using HagridAPI.Tests.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace HagridAPI.Tests.Controllers
{
    [TestClass]
    public class TestSiteController
    {
        private ISiteRepository _repository;

        public TestSiteController()
        {
            
        }

        public TestSiteController(ISiteRepository siteRepository)
        {
            _repository = siteRepository;
        }

        [TestMethod]
        public void GetReturnsSiteWithSameId()
        {
            // Arrange TODO: Use DI here!! don't instantiate sitetestrepository 
            var controller = new SiteController(new SiteTestRepository());

            // Act
            IHttpActionResult actionResult = controller.Get(1);
            var contentResult = actionResult as OkNegotiatedContentResult<Site>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(1, contentResult.Content.Id);
        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange TODO: Use DI here!! don't instantiate sitetestrepository 
            var controller = new SiteController(new SiteTestRepository());

            // Act
            IHttpActionResult actionResult = controller.Get(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
    }
}
