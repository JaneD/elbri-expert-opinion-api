﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HagridAPI.DAL.Models;
using HagridAPI.DAL.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HagridAPI.Tests.Repositories
{
   
    public class SiteTestRepository : ISiteRepository
    {
        private IQueryable<Site> fakeSites;

        // Dummy Data
        public SiteTestRepository()
        {
            fakeSites = new List<Site> {
                new Site()
                {
                    Id = 1,
                    Name = "Andys House",
                    LatLong = "BN11QD"
                }
            }.AsQueryable();
        }

        public Site GetSiteById(int id)
        {
            return fakeSites.FirstOrDefault(s => s.Id == id);
        }

        public Site[] GetAll()
        {
            return fakeSites.ToArray();
        }
    }
}
